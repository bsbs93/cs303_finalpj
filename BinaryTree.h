#ifndef _BIN_H
#define _BIN_H
using namespace std;

class BinaryTree
{
public:
	Node * Root; //creates root
	BinaryTree(); //constructor for Binary Tree, creates a new Node for root
	bool insertTree(string code, char letter) // function for building up the Morse Code tree
	{
		{
			if (Root == NULL) Root = new Node; // make a root if null, should never be the case
			Node * cursor = Root;
			while (code.size() > 1) // we will remove the front of the code string as we move along the tree path
			{
				if (code.front() == '.') // dot is left
				{
					code.erase(0, 1);
					if (cursor->left == NULL)cursor = (cursor->left = new Node); // if the child is null, make a new node, and set cursor to the new child
					else cursor = cursor->left; // if child has already been created, simply move the cursor to it (else we overwrite the existing data!)
				}
				else if (code.front() == '_') //underscore is right
				{
					code.erase(0, 1);
					if (cursor->right == NULL) cursor = (cursor->right = new Node);
					else cursor = cursor->right;
				}
				else return false;
			}
			// this is when the code string has been reduced to a single char ('.' or '_'); we will be placing data next
			if (code == ".") //set the left child to the character data
			{
				if (cursor->left == NULL) 
				{
					Node * toAdd = new Node;
					toAdd->data = letter; 
					cursor->left = toAdd;
				}
				else
				{
					cursor = cursor->left;
					cursor->data = letter;
				}

			}
			else if (code == "_") // or set the right child to the character data
			{
				if (cursor->right == NULL)
				{
					Node * toAdd2 = new Node;
					toAdd2->data = letter;
					cursor->right = toAdd2;
					return true;
				}
				else
				{
					cursor = cursor->right;
					cursor->data = letter;
					return true;
				}

			}
			else return false;
		}

	};// adds item to tree, returns true if succesful, or false if error
};

BinaryTree::BinaryTree()
{
	Root = new Node; // constructs Node for root of tree
};








#endif
