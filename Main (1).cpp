#include"Header.h"

using namespace std;

void main(void)
{
	//create Morse Manager object
	Manager MorseManager;
	
	//open morse file
	string file;
	ifstream fin("morse.txt");

	// validate file 
	if (!fin.good())cout << "Error! Add 'morse.txt' to source project folder\n";
	
	// prase through entire file 
	while (!fin.eof()) 
	{
		string line;
		getline(fin, line); // line from txt file is char followed by its code, no spaces
		char Key;
		Key = line.front(); // set Key to the character (first item in line)
		line.erase(line.begin()); // Remove char from line, and only the code remains
		MorseManager.EncodeMap[Key] = line; // Add key and line to map
		MorseManager.Tree.insertTree(line, Key); // Also insert character into the Morse Tree
	}

	// showcasing that it all works
	MorseManager.showCase("dog"); 
	MorseManager.showCase("monkey");
	MorseManager.showCase("hotdog");

	system("pause");
}