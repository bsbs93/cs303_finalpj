#ifndef _MORSE_
#define _MORSE_


class Manager
{
public:
	BinaryTree Tree; //Binary Tree holds Morse Code data (used for decoding)
	map<char, string> EncodeMap; // Also holds Morse Code data (only used for encoding)
	string encode(string input); // uses Map to turn characters into code
	string decode(string decode); // traverses Binary Tree to get character data
	void showCase(string codeWord); // takes in a word, encodes it using the encode function, and decodes it back using Morse Binary Tree
	void showMap(); // shows all the char:code mappings
};
void Manager::showMap()
{
	map<char, string>::const_iterator iter = EncodeMap.begin();
	while (iter != EncodeMap.end())
	{
		cout << iter->first << " " << iter->second << "\n";
		iter++;
	}
	return;
}

void Manager::showCase(string codeWord)
{
	cout << "Encodeing the word: " << codeWord << "\n";
	string code = encode(codeWord);
	cout << "The code for " << codeWord << " is: " << code << "\n";
	cout << "Using our morse code tree to decode: " << code << "\n";
	string decoded = decode(code); //decode function uses Morse Code Binary Tree to build word
	cout << "The code for " << decoded << " is " << code << "\n\n";
}
	// input must be a single word
string Manager::encode(string input) 
{
	istringstream stream(input); 
	string ret = ""; // string to concatenate to, and return at end
	string token; // token string to hold sting stream data
	while (stream >> token)
	{
		string::const_iterator it = token.begin(); // iterater for the string 
		map<char, string>::iterator Mit;
		while (it != token.end())
		{
			Mit = EncodeMap.find(*it);
			if (Mit != EncodeMap.end()) // use map to find code for each char value
			{
				ret += Mit->second += " "; // add code to return string
				it++;
			}
			else it++;
		}
	}
	return ret;
}

//output a single word
string Manager::decode(string output)
{
	istringstream stream(output);
	Node * cursor = Tree.Root; // make a cursor and set it to the Tree root
	string chunk; // chunk holds stream data
	string ret = ""; // string to return
	string::iterator iter;
	while (stream >> chunk) // for each code (represents a single char in a word)
	{
		iter = chunk.begin();
		while (chunk.size() > 1) // for each '.' or '_' in the code 
		{
			char j = chunk.front();
			if (j == '.')
			{
				cursor = cursor->left; // move left
				chunk.erase(0, 1); // erase front of code after we have moved
			}
			else if (j == '_')
			{
				cursor = cursor->right; // move right
				chunk.erase(0, 1);
			}
			else
			{
				chunk.erase(0, 1);
			}
		}
		// once code is reduced to single char, we find data in left or right child
		if (chunk == ".")
		{
			ret += cursor->left->data; 
			cursor = Tree.Root; // after char data is found, set cursor back to root for next code search
		}
		else if (chunk == "_")
		{
			ret += cursor->right->data;
			cursor = Tree.Root;
		}
		else
		{
			cout << "error: char not added to return string\n";
			cursor = Tree.Root;
		}
	}

	return ret;
}

























#endif